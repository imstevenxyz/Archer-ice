debug: false
log_level: info
dry_run: false
skip_consent: false

disks:
- device: /dev/nvme0n1
  table: gpt
  partitions:
  - name: boot
    config: "uefi fat32 1MiB 513MiB"
    flags: ["boot on", "esp on"]
    fs: vfat
  - name: lvm
    config: "lvm 513MiB 100%"
    flags: ["lvm"]

luks:
- name: luks_lvm
  device: /dev/nvme0n1p2
  volume_groups:
  - name: tsh
    volumes:
    - name: swap
      config: "-L 4G"
      fs: swap
    - name: root
      config: "-L 100G"
      fs: ext4
    - name: home
      config: "-l 100%FREE"
      fs: ext4

chroot:
  mount_point: /mnt
  mounts:
  - mount_point: /mnt
    device: /dev/tsh/root
  - mount_point: /mnt/home
    device: /dev/tsh/home
  - mount_point: /mnt/boot
    device: /dev/nvme0n1p1

system:
  packages:
    sort_mirrors: true
    pacman: [
      # Base
      "base", "base-devel", 
      "linux", "linux-firmware",

      # Filesystem
      "lvm2", "ntfs-3g",

      # Hardware
      "amd-ucode",

      # Networking
      "networkmanager", "openssh",
      "dnsutils", "inetutils", "traceroute",

      # Tooling
      "vim",
      "curl", "wget",
      "git", "gnupg",
      "man-db", "man-pages",
    ]

  initramfs:
    modules: [
      "vfio_pci", "vfio", "vfio_iommu_type1", "amdgpu"
    ]
    hooks: [
      "base", "udev", "autodetect", "modconf",
      "kms", "keyboard", "keymap", "block",
      "encrypt", "lvm2", "filesystems", "fsck"
    ]

  users:
  - name: mrmuffin
    groups: ["users", "adm", "wheel"]

  timezone: Europe/Amsterdam
  hwclock: true
  locale: "en_IE.UTF-8"
  keymap: us
  hostname: thespacehydra

  services:
  - systemd-timesyncd
  - fstrim.timer
  - NetworkManager

  extra_commands:
  - "sed -i \"/wheel ALL=(ALL:ALL) NOPASSWD: ALL/s/^#//g\" /etc/sudoers"

extensions:
  openssh:
    enable_server: true
    secure_config: true

  refind:
    config: |
      timeout 0
      extra_kernel_version_string linux linux-lts linux-hardened linux-zen
      include themes/rEFInd-minimal-black/theme.conf
    menuentries:
    - name: "Arch IOMMU RX6900XT"
      icon: "/EFI/refind/icons/os_arch.png"
      volume: "/dev/nvme0n1p2"
      options: "amd_iommu=on iommu=pt vfio-pci.ids=1002:73bf,1002:ab28 default_hugepagesz=1G hugepagesz=1G hugepages=16 rw cryptdevice=UUID=${VOLUME}:luks_lvm:allow-discards root=/dev/mapper/tsh-root initrd=amd-ucode.img"
    - name: "Arch fallback"
      icon: "/EFI/refind/icons/os_arch.png"
      volume: "/dev/nvme0n1p2"
      options: "rw cryptdevice=UUID=${VOLUME}:cryptlvm:allow-discards root=/dev/mapper/tsh-root initrd=amd-ucode.img"
