#!/bin/bash
# Dotfiles installer
# Author: imstevenxyz

set -e

USAGE="./install.sh {branch} [home]"

REPO="https://installer@gitlab.com/imstevenxyz/Archer-ice.git"
REPO_SSH="git@gitlab.com:imstevenxyz/Archer-ice.git"

verify_programs() {
  if ! [ -x "$(command -v git)" ]; then
    echo "Error: git is not installed!" >&2
    exit 1
  fi
}

verify_args() {
  if [ -z "$1" ]; then
    echo "Error: please specify a branch!"
    echo "$USAGE"
    exit 1
  fi
}

set_dir() {
  if [ -z "$LOC" ]; then
    DIR=$HOME
  else
    DIR=$LOC
  fi
}

info() {
  echo "Installing dotfiles with following options:"
  echo "branch: $BRANCH"
  echo "git-dir: $DIR/dotfiles"
  echo "work-tree: $DIR"
}

setup_bare_repo() {
  echo "Installing dotfiles"
  mkdir -p $DIR/dotfiles
  DF="/usr/bin/git --git-dir=$DIR/dotfiles --work-tree=$DIR"
  $DF clone --quiet --bare $REPO $DIR/dotfiles
  $DF config --local status.showUntrackedFiles no
  $DF checkout --quiet --force $BRANCH
  $DF remote set-url origin $REPO_SSH
}

setup_dotfiles() {
  SCRIPT="$DIR/.config/dotfiles/setup.sh"
  if [ -f "$SCRIPT" ]; then
    echo "Run '$SCRIPT $DIR' to continue setup!"
  else
    echo "Finished!"
  fi
}

#
# ENRYTPOINT
#

BRANCH=$1
LOC=$2

verify_programs
verify_args $@
set_dir
info
setup_bare_repo
setup_dotfiles